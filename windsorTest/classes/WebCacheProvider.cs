﻿using System;
using System.Web;

namespace windsorTest.classes
{
    public class WebCacheProvider : ICacheProvider
    {
        public void Put(string cacheKey, int cacheExpiryMinutes, object returnValue)
        {
            HttpRuntime.Cache.Insert(cacheKey, returnValue, null,
                DateTime.Now.AddMinutes(cacheExpiryMinutes), TimeSpan.Zero);
        }

        public object Get(string cacheKey)
        {
            return HttpRuntime.Cache[cacheKey];
        }
    }
}