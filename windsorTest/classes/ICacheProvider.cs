﻿namespace windsorTest.classes
{
    public interface ICacheProvider
    {
        void Put(string cacheKey, int cacheExpiryMinutes, object returnValue);
        object Get(string cacheKey);
    }
}