﻿namespace windsorTest.classes
{
    public class Details
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}