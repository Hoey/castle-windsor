using System;
using System.Linq;
using System.Web.Helpers;
using Castle.DynamicProxy;
using windsorTest.classes;

namespace windsorTest.Plumbing
{
    public class CacheInterceptor : IInterceptor
    {
        private readonly ICacheProvider _cacheProvider;

        private const int CacheExpiryMinutes = 1;

        public CacheInterceptor(ICacheProvider cacheProvider)
        {
            _cacheProvider = cacheProvider;
        }


        public void Intercept(IInvocation invocation)
        {
            if (invocation.TargetType.GetInterfaces().Any(x=>x.Name == "IRepo"))
            {


                //check if the method has a return value
                if (invocation.Method.ReturnType == typeof(void))
                {
                    invocation.Proceed();
                    return;
                }

                var cacheKey = BuildCacheKeyFrom(invocation);

                //try get the return value from the cache provider
                var item = _cacheProvider.Get(cacheKey);

                if (item != null)
                {
                    invocation.ReturnValue = item;
                    return;
                }

                //call the intercepted method
                invocation.Proceed();

                if (invocation.ReturnValue != null)
                {

                    _cacheProvider.Put(cacheKey, CacheExpiryMinutes, invocation.ReturnValue);
                }
            }
            else
            {
                invocation.Proceed();
            }
        }

        private static string BuildCacheKeyFrom(IInvocation invocation)
        {
            var methodName = invocation.Method.Name;

            var arguments = (from a in invocation.Arguments select a.ToString()).ToArray();
            var argsString = string.Join(",", arguments);

            var cacheKey = methodName + "-" + argsString;

            return cacheKey;

        }
    }

   
}