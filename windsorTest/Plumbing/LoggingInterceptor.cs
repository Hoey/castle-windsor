﻿using Castle.DynamicProxy;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;

namespace windsorTest.Plumbing
{
    public class LoggingInterceptor : IInterceptor
    {
        public void Intercept(IInvocation invocation)
        {
            Debug.WriteLine(
                $"Before method: {invocation.InvocationTarget}:{invocation.Method.Name}({JsonConvert.SerializeObject(invocation.Arguments)})");
            invocation.Proceed();
            Debug.WriteLine(
                $"After method: {invocation.InvocationTarget}:{invocation.Method.Name}({JsonConvert.SerializeObject(invocation.ReturnValue)})");
        }
    }
}