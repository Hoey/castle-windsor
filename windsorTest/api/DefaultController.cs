﻿using System.Web.Http;
using windsorTest.classes;
using windsorTest.Controllers;

namespace windsorTest.api
{
    public class DefaultController : ApiController
    {
        private readonly IService _service;

        public DefaultController(IService service)
        {
            _service = service;
        }

        public string Get()
        {
            return _service.GetMessage(new Details { Name = "paul", Description = "test"});
        }
    }
}
